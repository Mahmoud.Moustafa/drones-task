# Build Cmd

mvn clean install;

## Run Cmd

java -jar drones-task-1.0.0.jar


## Assumptions

Drone loading business logic:

1. User collect load items through UI.
2. User check available drones (Idle).
3. User load the Drone through API (Loading).
4. User change Drone state after physically loading the drone (Loaded).
5. User dispatch Drone (Delivering).
6. Drone/User confirm delivery (Delivered).
7. Drone signals return (Returning).
8. Drone returns to loading area (Idle).