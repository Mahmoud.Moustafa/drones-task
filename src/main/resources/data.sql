-- Drones
insert into drone (id, created, updated, serial_number, weight_limit, battery_level, model, state) values (1, current_timestamp, current_timestamp, '87342598762341', 500.0, 100.0, 'HEAVY_WEIGHT', 'IDLE');
insert into drone (id, created, updated, serial_number, weight_limit, battery_level, model, state) values (2, current_timestamp, current_timestamp, '87342598762342', 400.0, 100.0, 'CRUISER_WEIGHT', 'IDLE');
insert into drone (id, created, updated, serial_number, weight_limit, battery_level, model, state) values (3, current_timestamp, current_timestamp, '87342598762343', 300.0, 100.0, 'MIDDLE_WEIGHT', 'IDLE');
insert into drone (id, created, updated, serial_number, weight_limit, battery_level, model, state) values (4, current_timestamp, current_timestamp, '87342598762344', 200.0, 100.0, 'LIGHT_WEIGHT', 'IDLE');
insert into drone (id, created, updated, serial_number, weight_limit, battery_level, model, state) values (5, current_timestamp, current_timestamp, '87342598762345', 100.0, 100.0, 'LIGHT_WEIGHT', 'IDLE');
insert into drone (id, created, updated, serial_number, weight_limit, battery_level, model, state) values (6, current_timestamp, current_timestamp, '87342598762346', 500.0, 100.0, 'HEAVY_WEIGHT', 'IDLE');
insert into drone (id, created, updated, serial_number, weight_limit, battery_level, model, state) values (7, current_timestamp, current_timestamp, '87342598762347', 400.0, 100.0, 'CRUISER_WEIGHT', 'IDLE');
insert into drone (id, created, updated, serial_number, weight_limit, battery_level, model, state) values (8, current_timestamp, current_timestamp, '87342598762348', 300.0, 100.0, 'MIDDLE_WEIGHT', 'IDLE');
insert into drone (id, created, updated, serial_number, weight_limit, battery_level, model, state) values (9, current_timestamp, current_timestamp, '87342598762349', 200.0, 100.0, 'LIGHT_WEIGHT', 'IDLE');
insert into drone (id, created, updated, serial_number, weight_limit, battery_level, model, state) values (10, current_timestamp, current_timestamp, '87342598762350', 100.0, 100.0, 'LIGHT_WEIGHT', 'IDLE');

-- Medications
insert into medication (id, created, updated, name, weight, code, image_url) values (1, current_timestamp, current_timestamp, 'Medicine-1', 100.0, 'MIDICINE_1', 'http://localhost:8090/image/123456789.jpeg');

-- Users

-- insert into app_user values (1, current_timestamp, current_timestamp, 'mahmoud.moustafa.102@gmail.com', '{noop}password');
insert into user values (1, current_timestamp, current_timestamp, 'mahmoud.moustafa.102@gmail.com', '{bcrypt}$2a$10$WZDuUZOYdYRG1d257s9aw.Mli/l7Ih6bB0vxnwlsBQUr586EiICVC');

-- Roles

insert into role values (1, current_timestamp, current_timestamp, 'ROLE_USER');
insert into role values (2, current_timestamp, current_timestamp, 'ROLE_ADMIN');

-- Users Roles

insert into user_role values (1, 1);
insert into user_role values (1, 2);
