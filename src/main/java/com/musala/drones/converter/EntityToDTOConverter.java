package com.musala.drones.converter;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.Item;
import com.musala.drones.domain.Load;
import com.musala.drones.domain.Medication;
import com.musala.drones.dto.BatteryLevelDTO;
import com.musala.drones.dto.DroneDTO;
import com.musala.drones.dto.ItemDTO;
import com.musala.drones.dto.LoadDTO;
import com.musala.drones.dto.MedicationDTO;

@Component
public class EntityToDTOConverter {

	private ModelMapper modelMapper = new ModelMapper();



	public DroneDTO getDTO(Drone drone) {

		if (Objects.isNull(drone)) {
			return null;
		} else {
			return modelMapper.map(drone, DroneDTO.class);
		}
	}

	public MedicationDTO getDTO(Medication medication) {

		if (Objects.isNull(medication)) {
			return null;
		} else {
			return modelMapper.map(medication, MedicationDTO.class);
		}
	}

	public ItemDTO getDTO(Item item) {

		if (Objects.isNull(item)) {
			return null;
		} else {
			return modelMapper.map(item, ItemDTO.class);
		}
	}

	public LoadDTO getDTO(Load load) {

		if (Objects.isNull(load)) {

			return null;
		} else {

			return modelMapper.map(load, LoadDTO.class);
		}
	}

	public BatteryLevelDTO getBatteryLevelDTO(BigDecimal batteryLevel) {

		if (Objects.isNull(batteryLevel)) {
			return null;
		} else {
			return new BatteryLevelDTO(batteryLevel);
		}
	}

	public List<DroneDTO> getDTOList(List<Drone> drones) {

		if (Objects.isNull(drones) || drones.isEmpty()) {

			return Collections.emptyList();
		} else {

			return drones.stream().filter(Objects::nonNull).map(this::getDTO).collect(Collectors.toList());
		}
	}

	public Set<ItemDTO> getDTOSet(Set<Item> items) {

		if (Objects.isNull(items) || items.isEmpty()) {

			return Collections.emptySet();
		} else {

			return items.stream().filter(Objects::nonNull).map(this::getDTO).collect(Collectors.toSet());
		}
	}

}
