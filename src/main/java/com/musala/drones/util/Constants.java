package com.musala.drones.util;

public class Constants {

	public static final String MEDICATION_NAME_REGEX = "^[\\w-]+[^\\s\\W][\\w-]+$";

	public static final String MEDICATION_CODE_REGEX = "^[A-Z0-9_]+[^\\s\\W][A-Z0-9_]+$";
}
