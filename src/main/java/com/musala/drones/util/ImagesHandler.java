package com.musala.drones.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Component
public class ImagesHandler {

	private static final Logger LOG = LoggerFactory.getLogger(ImagesHandler.class);

	@Value("${application.imagesDir}")
	private String imagesDirectoryPath;

	private Path imagesDirectory;

	@PostConstruct
	public void init() {

		try {

			imagesDirectory = Paths.get(imagesDirectoryPath);

			if (!Files.exists(imagesDirectory)) {
				Files.createDirectory(imagesDirectory);
			}

		} catch (IOException e) {

			throw new RuntimeException("Could not create \"images\" directory!");
		}
	}

	public String saveImageFile(MultipartFile file) {

		String fileName = null;

		try {

			String contentType = file.getContentType();

			String extension = contentType.split("/")[1];

			fileName = String.valueOf(System.currentTimeMillis()) + "." + extension;

			Files.copy(file.getInputStream(), this.imagesDirectory.resolve(fileName)); // StandardCopyOption.REPLACE_EXISTING

			return fileName;

		} catch (Exception e) {

			LOG.error("Couldn't store file: {}", file.getOriginalFilename(), e);
			
			throw new RuntimeException("Couldn't store file: " + fileName, e);
		}
	}

	public Resource loadImageFile(String filename) {

		try {

			Path file = imagesDirectory.resolve(filename);
			Resource resource = new UrlResource(file.toUri());

			if (resource.exists() || resource.isReadable()) {

				return resource;
			} else {
				throw new RuntimeException("Could not read the file!");
			}

		} catch (MalformedURLException e) {
			throw new RuntimeException("Error: " + e.getMessage());
		}
	}
}
