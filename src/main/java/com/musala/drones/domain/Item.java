package com.musala.drones.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "item")
public class Item extends BaseEntity {

	@Column(name = "count")
	private Integer count;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "medication_id")
	private Medication medication;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "load_id")
	private Load load;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Medication getMedication() {
		return medication;
	}

	public void setMedication(Medication medication) {
		this.medication = medication;
	}

	public Load getLoad() {
		return load;
	}

	public void setLoad(Load load) {
		this.load = load;
	}

}
