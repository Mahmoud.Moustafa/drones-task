package com.musala.drones.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "load")
public class Load extends BaseEntity {

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinColumn(name = "drone_id")
	private Drone drone;

	@Enumerated(EnumType.STRING)
	private LoadState state;

	@OneToMany(mappedBy = "load", fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	private List<Item> items;

	public Drone getDrone() {
		return drone;
	}

	public void setDrone(Drone drone) {
		this.drone = drone;
	}

	public LoadState getState() {
		return state;
	}

	public void setState(LoadState state) {
		this.state = state;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

}
