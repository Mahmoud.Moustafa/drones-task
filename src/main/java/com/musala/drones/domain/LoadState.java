package com.musala.drones.domain;

public enum LoadState {

	CREATED, DELIVERING, DELIVERED;
}
