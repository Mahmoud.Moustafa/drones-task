package com.musala.drones.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import com.musala.drones.util.Constants;

@Entity
@Table(name = "medication")
public class Medication extends BaseEntity {

	@Pattern(regexp = Constants.MEDICATION_NAME_REGEX)
	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "weight", precision = 5, scale = 2, nullable = false)
	private Double weight;

	@Pattern(regexp = Constants.MEDICATION_CODE_REGEX)
	@Column(name = "code", nullable = false)
	private String code;

	@Column(name = "image_url", nullable = false)
	private String imageURL;

	@OneToMany(mappedBy = "medication")
	private Set<Item> items;



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

}
