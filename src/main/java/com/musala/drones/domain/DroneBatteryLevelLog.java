package com.musala.drones.domain;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;

@Entity
@Table(name = "battery_level_log")
public class DroneBatteryLevelLog extends BaseEntity {

	@DecimalMax(value = "100.00")
	@Column(name = "battery_level", precision = 5, scale = 2, nullable = false)
	private BigDecimal batteryLevel;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinColumn(name = "drone_id")
	private Drone drone;

	public BigDecimal getBatteryLevel() {
		return batteryLevel;
	}

	public void setBatteryLevel(BigDecimal batteryLevel) {
		this.batteryLevel = batteryLevel;
	}

	public Drone getDrone() {
		return drone;
	}

	public void setDrone(Drone drone) {
		this.drone = drone;
	}

}
