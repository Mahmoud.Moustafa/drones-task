package com.musala.drones.domain;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;

@Entity
@Table(name = "drone")
public class Drone extends BaseEntity {

	@Column(name = "serial_number", length = 100, nullable = false)
	private String serialNumber;

	@DecimalMax(value = "500.00")
	@Column(name = "weight_limit", precision = 5, scale = 2, nullable = false)
	private BigDecimal weightLimit;

	@DecimalMax(value = "100.00")
	@Column(name = "battery_level", precision = 5, scale = 2, nullable = false)
	private BigDecimal batteryLevel;

	@Enumerated(EnumType.STRING)
	@Column(name = "model", nullable = false)
	private DroneModel model;

	@Enumerated(EnumType.STRING)
	@Column(name = "state", nullable = false)
	private DroneState state;

	@OneToMany(mappedBy = "drone")
	private Set<Load> loads;



	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public BigDecimal getWeightLimit() {
		return weightLimit;
	}

	public void setWeightLimit(BigDecimal weightLimit) {
		this.weightLimit = weightLimit;
	}

	public BigDecimal getBatteryLevel() {
		return batteryLevel;
	}

	public void setBatteryLevel(BigDecimal batteryLevel) {
		this.batteryLevel = batteryLevel;
	}

	public DroneModel getModel() {
		return model;
	}

	public void setModel(DroneModel model) {
		this.model = model;
	}

	public DroneState getState() {
		return state;
	}

	public void setState(DroneState state) {
		this.state = state;
	}

	public Set<Load> getLoads() {
		return loads;
	}

	public void setLoads(Set<Load> loads) {
		this.loads = loads;
	}

	@Override
	public String toString() {
		return "Drone [id=" + id + ", created=" + created + ", updated=" + updated + ", serialNumber=" + serialNumber
				+ ", weightLimit=" + weightLimit + ", batteryLevel=" + batteryLevel + ", model=" + model
				+ ", state=" + state + "]";
	}

}
