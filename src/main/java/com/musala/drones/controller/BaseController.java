package com.musala.drones.controller;

import org.springframework.beans.factory.annotation.Autowired;

import com.musala.drones.converter.DtoToEntityConverter;
import com.musala.drones.converter.EntityToDTOConverter;

public class BaseController {

	@Autowired
	protected EntityToDTOConverter entityToDtoConverter;

	@Autowired
	protected DtoToEntityConverter dtoToEntityConverter;



	protected EntityToDTOConverter toDtoConverter() {

		return entityToDtoConverter;
	}

	protected DtoToEntityConverter toEntityConverter() {

		return dtoToEntityConverter;
	}
}
