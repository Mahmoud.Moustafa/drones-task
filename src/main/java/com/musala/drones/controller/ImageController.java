package com.musala.drones.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.musala.drones.util.ImagesHandler;

@RestController
@RequestMapping("/image")
public class ImageController {

	@Autowired
	private ImagesHandler imagesHandler;

	@GetMapping("/{name}")
	public ResponseEntity<Resource> getFile(@PathVariable String name) {

		Resource resource = imagesHandler.loadImageFile(name);

		if (Objects.nonNull(resource)) {
			return ResponseEntity.ok()
					.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
					.body(resource);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		
	}
}
