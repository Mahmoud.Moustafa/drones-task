package com.musala.drones.controller;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.musala.drones.dto.MedicationDTO;
import com.musala.drones.service.api.MedicationService;
import com.musala.drones.util.Constants;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/medications")
public class MedicationController extends BaseController {

	@Autowired
	private MedicationService medicationService;

	@ApiOperation(value = "Creates a new medication")
	@PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public MedicationDTO createMedication(
			@RequestParam("name") @Pattern(regexp = Constants.MEDICATION_NAME_REGEX) String name,
			@RequestParam("code") @DecimalMax(value = "500.00") String code,
			@RequestParam("weight") @Pattern(regexp = Constants.MEDICATION_CODE_REGEX) Double weight,
			@RequestPart MultipartFile image) {

		return toDtoConverter().getDTO(medicationService.saveMedication(name, code, weight, image));

	}

	@ApiOperation(value = "Updates a medication")
	@PutMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public MedicationDTO updateMedication(
			@RequestParam("id") Long id,
			@RequestParam("name") @Pattern(regexp = Constants.MEDICATION_NAME_REGEX) String name,
			@RequestParam("code") @DecimalMax(value = "500.00") String code,
			@RequestParam("weight") @Pattern(regexp = Constants.MEDICATION_CODE_REGEX) Double weight,
			@RequestPart MultipartFile image) {

		return toDtoConverter().getDTO(medicationService.saveMedication(name, code, weight, image));

	}

	@ApiOperation(value = "Retrieves a medication by ID")
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public MedicationDTO retrieveMedication(@PathVariable("id") Long id) {

		return toDtoConverter().getDTO(medicationService.getById(id));

	}

	@ApiOperation(value = "Deletes a medication by ID")
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteMedication(@PathVariable("id") Long id) {

		medicationService.delete(id);

		return ResponseEntity.ok().build();

	}
}
