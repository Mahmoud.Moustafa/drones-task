package com.musala.drones.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.musala.drones.dto.LoadDTO;
import com.musala.drones.service.api.LoadService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/load")
public class LoadController extends BaseController {

	@Autowired
	private LoadService LoadService;

//	@ApiOperation(value = "Creates a new load")
//	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	public LoadDTO createLoad(@RequestBody LoadDTO loadDTO) {
//
//		return toDtoConverter().getDTO(LoadService.createLoad(toEntityConverter().getEntity(loadDTO)));
//	}
}
