package com.musala.drones.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.musala.drones.domain.DroneState;
import com.musala.drones.dto.BatteryLevelDTO;
import com.musala.drones.dto.DroneDTO;
import com.musala.drones.dto.ItemDTO;
import com.musala.drones.dto.LoadDTO;
import com.musala.drones.service.api.DroneLoadingService;
import com.musala.drones.service.api.DroneService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/drones")
public class DroneController extends BaseController {

	@Autowired
	private DroneService droneService;

	@Autowired
	private DroneLoadingService droneLoadingService;



	@ApiOperation(value = "Creates a new drone")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public DroneDTO registerDrone(@RequestBody @Valid DroneDTO drone) {

		return toDtoConverter().getDTO(droneService.save(toEntityConverter().getEntity(drone)));
	}

	@ApiOperation(value = "Retrieves a drone by it's ID")
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public DroneDTO getDroneById(@PathVariable("id") Long droneId) {

		return toDtoConverter().getDTO(droneService.getById(droneId));
	}

	@ApiOperation(value = "Deletes a drone by it's ID")
	@DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> deleteDrone(@PathVariable("id") Long droneId) {

		droneService.delete(droneId);

		return ResponseEntity.ok().build();
	}

	@ApiOperation(value = "Retrieves a drone's battery level")
	@GetMapping(path = "/{id}/battery", produces = MediaType.APPLICATION_JSON_VALUE)
	public BatteryLevelDTO getDroneBatteryLevel(@PathVariable("id") Long droneId) {

		return toDtoConverter().getBatteryLevelDTO(droneService.getDroneBatteryLevel(droneId));
	}

	@ApiOperation(value = "Retrieves a list of drones in a specific state")
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<DroneDTO> getDronesByState(@RequestParam("state") DroneState state) {

		return toDtoConverter().getDTOList(droneService.getDronesByState(state));
	}

	@ApiOperation(value = "Loads a drone")
	@PostMapping(path = "/{id}/load", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public LoadDTO loadDrone(@PathVariable("id") Long droneId, @RequestBody @Valid List<ItemDTO> items) {

		return toDtoConverter().getDTO(droneLoadingService.loadDrone(droneId, toEntityConverter().getEntityList(items)));
	}

	@ApiOperation(value = "Retrieves a drone's current load")
	@GetMapping(path = "/{id}/load", produces = MediaType.APPLICATION_JSON_VALUE)
	public LoadDTO getCurrentLoad(@PathVariable("id") Long droneId) {

		return toDtoConverter().getDTO(droneLoadingService.getCurrentLoad(droneId));
	}
}
