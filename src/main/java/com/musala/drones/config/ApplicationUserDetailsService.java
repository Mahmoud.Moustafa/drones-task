package com.musala.drones.config;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.musala.drones.domain.Role;
import com.musala.drones.domain.User;
import com.musala.drones.repo.UserRepository;

@Service
public class ApplicationUserDetailsService implements UserDetailsService {

	@Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        final User dbUser = userRepository.findByEmail(username);

        if (dbUser == null) {
            throw new UsernameNotFoundException(username);
        }

        Set<String> roleSet = dbUser.getRoles().stream().filter(Objects::nonNull).map(Role::getName).collect(Collectors.toSet());

        String[] roles = roleSet.toArray(new String[roleSet.size()]);

        UserDetails user = org.springframework.security.core.userdetails.User.withUsername(dbUser.getEmail()).password(dbUser.getPassword()).authorities(roles).build();

        return user;
    }
}
