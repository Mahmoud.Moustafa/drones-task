package com.musala.drones.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class CustomBasicAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

	private static final String WWW_AUTHENTICATE_HEADER = "WWW-Authenticate";

	private static final String WWW_AUTHENTICATE_VALUE = "Basic realm=";

	private static final String BASIC_AUTHENTICATION_REALM = "Drones";

	@Override
	public void commence(final HttpServletRequest request, 
			final HttpServletResponse response, 
			final AuthenticationException authException) throws IOException, ServletException {

		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.addHeader(WWW_AUTHENTICATE_HEADER, WWW_AUTHENTICATE_VALUE + getRealmName());
		response.getWriter().flush();

	}

	@Override
	public void afterPropertiesSet() throws Exception {

		setRealmName(BASIC_AUTHENTICATION_REALM);

		super.afterPropertiesSet();
	}
}
