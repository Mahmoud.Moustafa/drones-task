package com.musala.drones.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.DroneState;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {

	Drone getBySerialNumber(String serialNumber);

	List<Drone> getByState(DroneState state);
}
