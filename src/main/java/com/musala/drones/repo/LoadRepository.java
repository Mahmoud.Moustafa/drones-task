package com.musala.drones.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.musala.drones.domain.Load;
import com.musala.drones.domain.LoadState;

@Repository
public interface LoadRepository extends JpaRepository<Load, Long> {

	Load getLoadByDroneIdAndStateIn(Long droneId, List<LoadState> state);
}
