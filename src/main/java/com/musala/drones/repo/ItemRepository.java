package com.musala.drones.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.musala.drones.domain.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

}
