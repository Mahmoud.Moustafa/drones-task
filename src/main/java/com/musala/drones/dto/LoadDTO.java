package com.musala.drones.dto;

import java.util.Set;

import com.musala.drones.domain.LoadState;

public class LoadDTO extends EntityDTO {

	private DroneDTO drone;

	private LoadState state;

	private Set<ItemDTO> items;

	public DroneDTO getDrone() {
		return drone;
	}

	public void setDrone(DroneDTO drone) {
		this.drone = drone;
	}

	public LoadState getState() {
		return state;
	}

	public void setState(LoadState state) {
		this.state = state;
	}

	public Set<ItemDTO> getItems() {
		return items;
	}

	public void setItems(Set<ItemDTO> items) {
		this.items = items;
	}

}
