package com.musala.drones.dto;

import java.math.BigDecimal;

public class BatteryLevelDTO extends BaseDTO{

	private BigDecimal level;

	public BatteryLevelDTO() {}

	public BatteryLevelDTO(BigDecimal level) {

		this.level = level;
	}

	public BigDecimal getLevel() {
		return level;
	}

	public void setLevel(BigDecimal level) {
		this.level = level;
	}

}
