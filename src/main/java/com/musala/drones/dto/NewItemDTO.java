package com.musala.drones.dto;

public class NewItemDTO extends BaseDTO {

	private Long medicationId;

	private Integer count;

	public Long getMedicationId() {
		return medicationId;
	}

	public void setMedicationId(Long medicationId) {
		this.medicationId = medicationId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	
}
