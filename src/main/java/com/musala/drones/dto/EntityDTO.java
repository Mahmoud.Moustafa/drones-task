package com.musala.drones.dto;

public abstract class EntityDTO extends BaseDTO {

	protected Long id;

	protected Long created;

	protected Long updated;



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Long getUpdated() {
		return updated;
	}

	public void setUpdated(Long updated) {
		this.updated = updated;
	}

}
