package com.musala.drones.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.musala.drones.domain.DroneModel;
import com.musala.drones.domain.DroneState;

public class DroneDTO extends EntityDTO {

	@NotBlank
	private String serialNumber;

	@DecimalMax(value = "500.00")
	private BigDecimal weightLimit;

	@DecimalMax(value = "100.00")
	private BigDecimal batteryLevel;

	@NotNull
	private DroneModel model;

	@NotNull
	private DroneState state;

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public BigDecimal getWeightLimit() {
		return weightLimit;
	}

	public void setWeightLimit(BigDecimal weightLimit) {
		this.weightLimit = weightLimit;
	}

	public BigDecimal getBatteryLevel() {
		return batteryLevel;
	}

	public void setBatteryLevel(BigDecimal batteryLevel) {
		this.batteryLevel = batteryLevel;
	}

	public DroneModel getModel() {
		return model;
	}

	public void setModel(DroneModel model) {
		this.model = model;
	}

	public DroneState getState() {
		return state;
	}

	public void setState(DroneState state) {
		this.state = state;
	}

}
