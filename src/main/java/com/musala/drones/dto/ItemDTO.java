package com.musala.drones.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ItemDTO extends EntityDTO {

	@Min(value = 1)
	private Integer count;

	@NotNull
	private MedicationDTO medication;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public MedicationDTO getMedication() {
		return medication;
	}

	public void setMedication(MedicationDTO medication) {
		this.medication = medication;
	}

}
