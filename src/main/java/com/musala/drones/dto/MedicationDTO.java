package com.musala.drones.dto;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.musala.drones.util.Constants;

public class MedicationDTO extends EntityDTO {

	@NotNull
	@Pattern(regexp = Constants.MEDICATION_NAME_REGEX)
	private String name;

	@NotNull
	@DecimalMax(value = "500.00")
	private Double weight;

	@NotNull
	@Pattern(regexp = Constants.MEDICATION_CODE_REGEX)
	private String code;

	@NotNull
	private String imageURL;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

}
