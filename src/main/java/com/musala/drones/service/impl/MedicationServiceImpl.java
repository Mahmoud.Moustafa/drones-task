package com.musala.drones.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.musala.drones.domain.Medication;
import com.musala.drones.repo.MedicationRepository;
import com.musala.drones.service.api.MedicationService;
import com.musala.drones.util.ImagesHandler;

@Service
public class MedicationServiceImpl implements MedicationService {

	private static final Logger LOG = LoggerFactory.getLogger(MedicationServiceImpl.class);

	@Value("${application.imagesUrlTemplate}")
	private String imagesUrlPattern;

	@Autowired
	private MedicationRepository medicationRepository;

	@Autowired
	private ImagesHandler imagesHandler;


	@Override
	public Medication saveMedication(String name, String code, Double weight, MultipartFile image) {

		Medication medication = new Medication();
		medication.setName(name);
		medication.setCode(code);
		medication.setWeight(weight);
		medication.setImageURL(String.format(imagesUrlPattern, imagesHandler.saveImageFile(image)));

		return medicationRepository.save(medication);
	}

	@Override
	public Medication updateMedication(Long id, String name, String code, Double weight, MultipartFile image) {

		Medication medication = medicationRepository.getOne(id);

		medication.setName(name);
		medication.setCode(code);
		medication.setWeight(weight);
		medication.setImageURL(String.format(imagesUrlPattern, imagesHandler.saveImageFile(image)));

		return medicationRepository.save(medication);
	}

	@Override
	public Medication getById(Long id) {

		Medication medication = medicationRepository.findById(id).orElse(null);

		return medication;
	}

	@Override
	public void delete(Long id) {

		Medication medication = medicationRepository.findById(id).orElse(null);

		medicationRepository.delete(medication);
	}

}
