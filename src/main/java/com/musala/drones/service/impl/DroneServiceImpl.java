package com.musala.drones.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.DroneBatteryLevelLog;
import com.musala.drones.domain.DroneState;
import com.musala.drones.repo.DroneBatteryLevelLogRepository;
import com.musala.drones.repo.DroneRepository;
import com.musala.drones.service.api.DroneService;

@Service
public class DroneServiceImpl implements DroneService {

	private static final Logger LOG = LoggerFactory.getLogger(DroneServiceImpl.class);

	@Autowired
	private DroneRepository droneRepository;

	@Autowired
	private DroneBatteryLevelLogRepository batteryLevelLogRepository;

	@Value("${application.drone-battery-level-check-enabled}")
	private boolean batteryCheckEnabled;

	@Override
	public Drone save(Drone drone) {

		Drone droneBySN = droneRepository.getBySerialNumber(drone.getSerialNumber());

		if (Objects.nonNull(droneBySN)) {
			throw new IllegalArgumentException(String.format("Drone with serial no. %s already exists", drone.getSerialNumber()));
		}

		return droneRepository.save(drone);
	}

	@Override
	public Drone update(Drone drone) {
		
		return droneRepository.save(drone);
	}

	@Override
	public Drone getById(Long droneId) {

		return droneRepository.findById(droneId).orElse(null);
	}

	@Override
	public void delete(Long droneId) {

		Drone drone = droneRepository.getOne(droneId);

		droneRepository.delete(drone);
	}

	@Override
	public List<Drone> getDronesByState(DroneState droneState) {

		return droneRepository.getByState(droneState);
	}

	@Override
	public BigDecimal getDroneBatteryLevel(Long droneId) {

		Drone drone = getById(droneId);

		return drone.getBatteryLevel();
	}

	@Scheduled(fixedDelayString = "${application.drone-battery-level-check-ms}")
	public void checkDronesBatteryLevel() {

		if (batteryCheckEnabled) {

			Page<Drone> page = null;

			int i = 0;

			do {

				page = droneRepository.findAll(PageRequest.of(0, 5));

				page.getContent().stream().forEach(drone -> {

					DroneBatteryLevelLog batteryLog = new DroneBatteryLevelLog();

					batteryLog.setDrone(drone);
					batteryLog.setBatteryLevel(drone.getBatteryLevel());

					batteryLevelLogRepository.save(batteryLog);
				});

				i++;

			} while (i < page.getTotalPages());
		}
	}

}
