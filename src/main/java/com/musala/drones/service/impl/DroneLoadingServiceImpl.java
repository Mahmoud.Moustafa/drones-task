package com.musala.drones.service.impl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.DroneState;
import com.musala.drones.domain.Item;
import com.musala.drones.domain.Load;
import com.musala.drones.domain.LoadState;
import com.musala.drones.domain.Medication;
import com.musala.drones.service.api.DroneLoadingService;
import com.musala.drones.service.api.DroneService;
import com.musala.drones.service.api.ItemService;
import com.musala.drones.service.api.LoadService;
import com.musala.drones.service.api.MedicationService;

@Service
public class DroneLoadingServiceImpl implements DroneLoadingService {

	@Autowired
	private DroneService droneService;

	@Autowired
	private MedicationService medicationService;

	@Autowired
	private LoadService loadService;

	@Autowired
	private ItemService itemService;

	@Value("${application.drone-loading-min-battery-level}")
	private BigDecimal loadingMinBatteryLevel;



	@Override
	public Load loadDrone(Long droneId, List<Item> items) {

		Drone drone = droneService.getById(droneId);

		if (!DroneState.IDLE.equals(drone.getState())) {
			throw new IllegalStateException("Drone not in IDLE state");
		}

		if (loadingMinBatteryLevel.compareTo(drone.getBatteryLevel()) > 0) {

			throw new IllegalStateException("Drone battery low");
		}

		List<Item> preparedItems = prepareItems(drone.getWeightLimit(), items);

		drone.setState(DroneState.LOADING);

		Load load = new Load();
		load.setDrone(drone);
		load.setState(LoadState.CREATED);
		load.setItems(preparedItems);

		load = loadService.createLoad(load);

		for (Item item : preparedItems) {
			item.setLoad(load);
		}

		itemService.saveAll(preparedItems);

		return load;
	}

	private List<Item> prepareItems(BigDecimal maxWeight, List<Item> items) {

		List<Item> preparedItems = new LinkedList<>();

		double totalWeight = 0.0;

		Item preparedItem = null;

		for (Item item : items) {

			preparedItem = prepareItem(item.getMedication().getId(), item.getCount());

			totalWeight += preparedItem.getMedication().getWeight() * preparedItem.getCount();

			preparedItems.add(preparedItem);
		}

		if (totalWeight > maxWeight.doubleValue()) {
			throw new IllegalStateException(String.format("Drone over weight, max: %s load: %s", maxWeight, totalWeight));
		}

		return preparedItems;
	}

	private Item prepareItem(Long medicationId, Integer count) {

		Medication medication = medicationService.getById(medicationId);

		if (Objects.isNull(medication)) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Medicine with id = %s not found", medicationId));
		}

		if (count <= 0) {
			throw new IllegalArgumentException(String.format("Invalid items count: %s", count));
		}

		Item item = new Item();
		item.setMedication(medication);
		item.setCount(count);

		return item;
	}

	@Override
	public Load getCurrentLoad(Long droneId) {

		Load currentLoad = loadService.getLoadByDroneIdAndStateIn(droneId, Arrays.asList(LoadState.CREATED, LoadState.DELIVERING));

		if (Objects.isNull(currentLoad)) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}

		return currentLoad;
	}

}
