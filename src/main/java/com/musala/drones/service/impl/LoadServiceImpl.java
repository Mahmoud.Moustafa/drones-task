package com.musala.drones.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.musala.drones.domain.Load;
import com.musala.drones.domain.LoadState;
import com.musala.drones.repo.LoadRepository;
import com.musala.drones.service.api.LoadService;

@Service
public class LoadServiceImpl implements LoadService {

	private static final Logger LOG = LoggerFactory.getLogger(LoadServiceImpl.class);

	@Autowired
	private LoadRepository loadRepository;

	@Override
	public Load createLoad(Load load) {

		return loadRepository.save(load);
	}

	@Override
	public Load getLoadByDroneIdAndStateIn(Long droneId, List<LoadState> state) {

		return loadRepository.getLoadByDroneIdAndStateIn(droneId, state);
	}

}
