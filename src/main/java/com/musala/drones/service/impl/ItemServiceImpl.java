package com.musala.drones.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.musala.drones.domain.Item;
import com.musala.drones.repo.ItemRepository;
import com.musala.drones.service.api.ItemService;

@Service
public class ItemServiceImpl implements ItemService {

	private static final Logger LOG = LoggerFactory.getLogger(ItemServiceImpl.class);

	@Autowired
	private ItemRepository itemRepository;

	@Override
	public Item save(Item item) {

		return itemRepository.save(item);
	}

	@Override
	public List<Item> saveAll(List<Item> item) {

		return itemRepository.saveAll(item);
	}

}
