package com.musala.drones.service.api;

import java.util.List;

import com.musala.drones.domain.Item;

public interface ItemService {

	Item save(Item item);

	List<Item> saveAll(List<Item> item);
}
