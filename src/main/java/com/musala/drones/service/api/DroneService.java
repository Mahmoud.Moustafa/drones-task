package com.musala.drones.service.api;

import java.math.BigDecimal;
import java.util.List;

import com.musala.drones.domain.Drone;
import com.musala.drones.domain.DroneState;

public interface DroneService {

	Drone save(Drone drone);

	Drone update(Drone drone);

	Drone getById(Long droneId);

	void delete(Long droneId);

	BigDecimal getDroneBatteryLevel(Long droneId);

	List<Drone> getDronesByState(DroneState droneState);
}
