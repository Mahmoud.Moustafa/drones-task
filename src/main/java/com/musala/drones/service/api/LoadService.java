package com.musala.drones.service.api;

import java.util.List;

import com.musala.drones.domain.Load;
import com.musala.drones.domain.LoadState;

public interface LoadService {

	Load createLoad(Load load);

	Load getLoadByDroneIdAndStateIn(Long droneId, List<LoadState> state);
}
