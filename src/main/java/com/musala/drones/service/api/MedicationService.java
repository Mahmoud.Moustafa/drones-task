package com.musala.drones.service.api;

import org.springframework.web.multipart.MultipartFile;

import com.musala.drones.domain.Medication;

public interface MedicationService {

	Medication saveMedication(String name, String code, Double weight, MultipartFile image);

	Medication updateMedication(Long id, String name, String code, Double weight, MultipartFile image);

	Medication getById(Long id);

	void delete(Long id);

}
