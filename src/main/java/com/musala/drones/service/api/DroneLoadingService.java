package com.musala.drones.service.api;

import java.util.List;

import com.musala.drones.domain.Item;
import com.musala.drones.domain.Load;

public interface DroneLoadingService {

	Load loadDrone(Long droneId, List<Item> items);

	Load getCurrentLoad(Long droneId);
}
